from taptap.snippet import SnippetToRefactor


if __name__ == "__main__":
    snippet = SnippetToRefactor()
    snippet.send('SMS', 'message', 'to', 'subject')
    snippet.send('mail', 'message', 'to', 'subject')
    snippet.send('another', 'message', 'to', 'subject')
