from smpp import Smpp
from sendmail import SendMailConnection

class SnippetToRefactor(object):	
    def send(self, type, message, to, subject):
        if type == 'SMS':
            smpp = Smpp('192.168.1.4', '4301')
            smpp.open('your-username', 'your-password')
            smpp.send(to, message)
            smpp.close()
        elif type == 'mail':
            smc = SendMailConnection('mail.myserver.com')
            smc.prepare_message(to, message, subject)
            smc.send()
            smc.close()
        else:
            raise Exception('Invalid type')
